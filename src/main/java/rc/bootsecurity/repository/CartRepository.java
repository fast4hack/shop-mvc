package rc.bootsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rc.bootsecurity.model.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {
    Cart findCartById(Integer id);
}
