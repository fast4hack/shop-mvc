package rc.bootsecurity.db;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rc.bootsecurity.model.Product;
import rc.bootsecurity.model.User;
import rc.bootsecurity.repository.ProductRepository;
import rc.bootsecurity.repository.UserRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class DbInit implements CommandLineRunner {
    private static final String imageUrl = "https://images-na.ssl-images-amazon.com/images/I/71sNNCTfMuL._SL1500_.jpg";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ProductRepository productRepository;

    public DbInit(UserRepository userRepository, PasswordEncoder passwordEncoder, ProductRepository productRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.productRepository = productRepository;
    }

    @Override
    public void run(String... args) {
        // Delete all
        this.userRepository.deleteAll();

        // Crete users
        User dan = new User("user", passwordEncoder.encode("123"), "USER", "");
        User admin = new User("admin", passwordEncoder.encode("admin"), "ADMIN", "ACCESS_TEST1,ACCESS_TEST2");
        User manager = new User("manager", passwordEncoder.encode("manager123"), "MANAGER", "ACCESS_TEST1");

        List<User> users = Arrays.asList(dan, admin, manager);

        // Save to db
        this.userRepository.saveAll(users);

        List<Product> products = IntStream.range(1, 4)
                .mapToObj(i -> {
                    return new Product(null, "Product #" + i,
                            "This is description for Product #" + i, imageUrl);
                })
                .collect(Collectors.toList());
        productRepository.saveAll(products);
    }
}
