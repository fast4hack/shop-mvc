/*
 * Copyright 2019 Alexander Kosarev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rc.bootsecurity.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import rc.bootsecurity.model.Product;
import rc.bootsecurity.service.ProductService;

import java.util.Map;

@Controller
@RequestMapping("/catalog")
@RequiredArgsConstructor
public class ProductsController {

    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    public ModelAndView list() {
        return new ModelAndView("/list",
                Map.of("products", productService.getProducts()), HttpStatus.OK);
    }

    @GetMapping("/{productId}")
    public ModelAndView product(@PathVariable Integer productId) {
        Product productById = productService.getProductById(productId);
        if (productById != null) {
            return new ModelAndView("/product",
                    Map.of("product", productById), HttpStatus.OK);
        } else {
            return new ModelAndView("errors/404",
                    Map.of("error", "Couldn't find a product"), HttpStatus.NOT_FOUND);
        }
    }
}
