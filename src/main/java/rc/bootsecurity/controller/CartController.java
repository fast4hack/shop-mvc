package rc.bootsecurity.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import rc.bootsecurity.model.Cart;
import rc.bootsecurity.model.Product;
import rc.bootsecurity.model.User;
import rc.bootsecurity.security.UserPrincipal;
import rc.bootsecurity.service.CartService;

import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class CartController {

    @Autowired
    private CartService cartService;

    @PostMapping("/cart/add/{productId}")
    public ModelAndView addProduct(@PathVariable("productId") Integer productId) {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        Cart cart = cartService.addProductToCart(user.getId(), productId);
        List<Product> products = cartService.getProductsInTheCart(cart);
        return new ModelAndView("/cart/cart",
                Map.of("products", products), HttpStatus.OK);
    }

    @GetMapping("/cart")
    public ModelAndView cart() {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        Cart cart = cartService.getCartById(user.getId());
        List<Product> products = cartService.getProductsInTheCart(cart);

        return new ModelAndView("/cart/cart",
                Map.of("products", products), HttpStatus.OK);
    }

    @PostMapping("cart/remove/{productId}")
    public ModelAndView remove(@PathVariable("productId") Integer productId) {

        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        Cart cart = cartService.removeProductFromCart(user.getId(), productId);
        List<Product> products = cartService.getProductsInTheCart(cart);
        return new ModelAndView("/cart/cart",
                Map.of("products", products), HttpStatus.OK);
    }

}
